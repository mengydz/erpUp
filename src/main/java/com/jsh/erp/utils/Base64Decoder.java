package com.jsh.erp.utils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * @ClassName: Base64Decoder
 * @ClassNameExplain: base64解码工具类
 * @author zhangfangming
 * @date 2016年8月2日
 */
public class Base64Decoder {

    /**
     * @param msgContent 需要解码字符
     * @return
     * @Title: getFromBASE64
     * @TitleExplain:
     * @Description: 将base64加密字符解码
     * @version
     * @author zhangfangming-mac
     */
    public static String getFromBASE64(String msgContent) {
        if (msgContent == null) {
            return null;
        }
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            byte[] b = decoder.decodeBuffer(msgContent);
            return new String(b, "utf-8");
        } catch (Exception e) {
            return null;
        }

    }


    /**
     * @param msgContent 需要加密字符
     * @return
     * @Title: toBase64
     * @TitleExplain:
     * @Description: 使用base64加密字符
     * @version
     * @author zhangfangming-mac
     */
    public static String toBase64(String msgContent){
        String ret = null;
        ret = new BASE64Encoder().encode(msgContent.getBytes());
        System.out.println("加密前:"+msgContent+" 加密后:"+ret);
        try {
        ret = new String(new BASE64Decoder().decodeBuffer(msgContent));
        } catch (Exception e) {
            return null;
        }
        return ret;
    }

}
